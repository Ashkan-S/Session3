package parsa.session3;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class SecondActivity extends AppCompatActivity {

    TextView finalMessage;
    MainActivity startAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        startAct = new MainActivity();
        String name, family;
        int age, birthday;

        finalMessage = (TextView) findViewById(R.id.finalTXT);

//        name = startAct.ShowPref("name", "");
//        family = startAct.ShowPref("family", "");
//        age = Integer.parseInt(startAct.ShowPref("age", "0"));

        name = PreferenceManager.getDefaultSharedPreferences(this).getString("name", "");
        family = PreferenceManager.getDefaultSharedPreferences(this).getString("family", "");
        try {
            age = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("age", ""));

            birthday = 1396 - age;
            finalMessage.setText("Hello dear " + name + " " + family + "\n" +
                    "I think you have born in " + birthday + " !");
        } catch (Exception e) {
            age = 0;
            finalMessage.setText("You haven't enter your age." + "\n" +
                    "Please go back & enter your age!");
        }
        ;
    }
}
