package parsa.session3;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button next;

    EditText name;
    EditText family;
    EditText age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        next = (Button) findViewById(R.id.nextBTN);

        name = (EditText) findViewById(R.id.nameTXT);
        family = (EditText) findViewById(R.id.familyTXT);
        age = (EditText) findViewById(R.id.ageTXT);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameValue = name.getText().toString();
                String familyValue = family.getText().toString();
                String ageValue = age.getText().toString();

                SavePref("name",nameValue);
                SavePref("family",familyValue);
                SavePref("age",ageValue);

                ShowToast("Saved!");

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(intent);
//                finish();
            }
        });
    }

    public void ShowToast (String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public String ShowPref(String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getString(key, defaultValue);
    }

    public void SavePref(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit()
                .putString(key, value).commit();
    }
}
